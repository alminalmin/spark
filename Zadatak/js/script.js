/******************************
 ******* Google maps **********
 ******************************/
function initialize() {
    var mapProp = {
        center: new google.maps.LatLng(43.352914, 17.793048),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false
    };

    var image = {
        url: 'css/images/icon-map.png',
        size: new google.maps.Size(64, 64)
    };

    var map = new google.maps.Map(document.getElementById("contact-map"), mapProp);

    var marker = new google.maps.Marker({
        map: map,
        position: mapProp.center,
        animation: google.maps.Animation.DROP,
        title: "NSoft, Bleiburških žrtava, Mostar",
        icon: image,
        scrollwheel: mapProp.scrollwheel
    });
}

google.maps.event.addDomListener(window, 'load', initialize);


/*************************
******* Navigation *******
**************************/
// Set active class on navigation element related to scroll position
function setActiveClass(position, elemPosion, elems) {
    if (position < elemPosion.about) {
        elems[0].classList.add('active');
    } else if (position >= elemPosion.about && position < elemPosion.contact) {
        elems[1].classList.add('active');
    } else if (position >= elemPosion.contact) {
        elems[3].classList.add('active');
    }
}

// Remove active class from elements
function removeActiveClass(items) {
    var len = items.length,
        i;

    for (i = 0; i < len; i++) {
        items[i].classList.remove('active');
    }
}

// Store navigation elements
var nav = document.getElementsByClassName('main-menu')[0].children[0],
    navLinks = nav.getElementsByTagName('a');

var navElemsPosition = {
    home: document.getElementById('home').offsetTop - 87,
    about: document.getElementById('about').offsetTop - 75,
    contact: document.getElementById('contact').offsetTop - 300
};


// Bind click events on navigation elements
Object.keys(navLinks).forEach(function (key) {
    var elem = navLinks[key];

    elem.addEventListener('click', function (e) {
        var hash = e.target.hash,
            positon;

        if (!hash) return;

        e.preventDefault();

        removeActiveClass(navLinks); // Remove active class
        this.classList.add('active'); // Add active class
        
        positon = navElemsPosition[hash.substring(1, hash.length)];
        window.scrollTo(0, positon); // Scroll to element position
    });
});

// Update navigation active class on scroll position
var scrollPos = window.scrollY,
    timeout;

setActiveClass(scrollPos, navElemsPosition, navLinks); // Set active class
window.addEventListener('scroll', function (e) {
    clearTimeout(timeout);

    timeout = setTimeout(function () {
        scrollPos = window.scrollY; // Update scroll position
       
        removeActiveClass(navLinks); // Remove active class
        setActiveClass(scrollPos, navElemsPosition, navLinks); // Set active class
    }, 250);
});


/*************************
******* Rating *******
**************************/

var ratingElems = document.getElementsByClassName('contact-item-rating-item');

// Bind click event to set active class
Object.keys(ratingElems).forEach(function(key) {
    var elem = ratingElems[key];
    
    elem.onclick = function (e) {
        var len, i;
        
        // Remove active class
        removeActiveClass(ratingElems);
        
        // Add active class
        len = parseInt(key, 10) + 1;
        for (i = 0; i < len; i++) {
            ratingElems[i].classList.add('active');
        }
    }
});

function updateImage (){
           var imagesCount = this.images.path.length -1;
           
           this.images.position;
           this.images.poster; //ovdje mislim da fali nesto
           
       }
       
       var images = {
           path: [
               'css/images/poster.jpg',
                
                'css/images/poster2.jpg'
           ],
           position:0,
           poster: document.getElementsByClassName('poster-slide')[0]
       };
       
       //bind click events on previous/next arrows
       var arrows =  document.getElementsByClassName('poster-navigation');
       arrows[0].onclick = updateImage.bind({images, direction: 'previos'});
      arrows[1].onclick = updateImage.bind({images, direction: 'next'}); 